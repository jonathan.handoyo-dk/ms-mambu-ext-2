package com.jago.mambu

import com.fasterxml.jackson.databind.ObjectMapper
import com.jago.mambu.service.mambu.MambuService
import mu.KotlinLogging
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener
import java.time.LocalDate

@ConfigurationPropertiesScan
@SpringBootApplication
class Application(
    private val mambuService: MambuService,
    private val objectMapper: ObjectMapper,
) {

    private val logger = KotlinLogging.logger {}

    @EventListener(ApplicationReadyEvent::class)
    fun onApplicationReadyEvent(event: ApplicationReadyEvent) {
        logger.info { "Application started!!!" }
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
