package com.jago.mambu

import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

val FORMAT_DATE_FULL: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.systemDefault())

fun LocalDate.toInstant(): Instant = this.atStartOfDay(ZoneId.systemDefault()).toInstant()
fun Instant.toIsoOffsetDateTime(): String = FORMAT_DATE_FULL.format(this)
fun Instant.toLocalDate(): LocalDate = LocalDate.ofInstant(this, ZoneId.systemDefault())
fun String.toInstant(): Instant = Instant.parse(this)