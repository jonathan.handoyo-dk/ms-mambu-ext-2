package com.jago.mambu.service

import com.jago.mambu.service.mambu.models.PaginationDetails
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpHeaders

inline fun <reified T> typeReference(): ParameterizedTypeReference<T> = object : ParameterizedTypeReference<T>() {}

fun HttpHeaders.getPaginationDetails(): PaginationDetails = PaginationDetails(this)