package com.jago.mambu.service.mambu

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@ConfigurationProperties("app.services.mambu")
data class MambuProperties(
    val api: Api,
    val timeout: Timeout = Timeout(),
    val scheduler: Scheduler = Scheduler()
) {
    data class Api(
        val url: String,
        val key: String,
        val limit: Int = 100
    )

    data class Timeout(
        val connect: Long = 5_000,
        val response: Long = 5_000,
        val read: Long = 5_000,
        val write: Long = 5_000
    )

    data class Scheduler(
        val expiry: Int = 5,
        val maxData: MaxData = MaxData()
    ) {
        data class MaxData(
            val accounts: Int = 500,
            val clients: Int = 1_000,
            val journal: Int = 10_000,
            val jobs: Int = 2_000,
            val loanDays: Int = 1
        )
    }
}

@Configuration
class MambuConfig(
    private val restTemplateBuilder: RestTemplateBuilder,
) {

    @Bean
    fun mambuRestTemplate(): RestTemplate {
        return restTemplateBuilder.build()
    }
}