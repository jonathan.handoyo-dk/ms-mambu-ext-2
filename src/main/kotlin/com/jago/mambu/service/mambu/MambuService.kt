package com.jago.mambu.service.mambu

import com.jago.mambu.service.getPaginationDetails
import com.jago.mambu.service.mambu.dtos.*
import com.jago.mambu.service.mambu.enums.DetailsLevel
import com.jago.mambu.service.mambu.enums.Operator
import com.jago.mambu.service.mambu.enums.Toggle
import com.jago.mambu.service.mambu.models.PaginationDetails
import com.jago.mambu.service.typeReference
import com.jago.mambu.toInstant
import com.jago.mambu.toIsoOffsetDateTime
import mu.KotlinLogging
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpStatusCodeException
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.exchange
import org.springframework.web.util.UriComponentsBuilder
import java.time.LocalDate

@Service
class MambuService(
    private val mambuRestTemplate: RestTemplate,
    private val mambuProperties: MambuProperties,
) {

    private val logger = KotlinLogging.logger {}

    private val headers = HttpHeaders().also {
        it.set(HttpHeaders.ACCEPT, "application/vnd.mambu.v2+json")
        it.set(HttpHeaders.CONTENT_TYPE, "application/json")
        it.set("ApiKey", mambuProperties.api.key)
    }

    private fun exchangeForPaginationDetails(path: String, criteria: SearchCriteria?): PaginationDetails {
        val url = UriComponentsBuilder
            .fromHttpUrl(mambuProperties.api.url)
            .path(path)
            .queryParam("offset", 0)
            .queryParam("limit", 1)
            .queryParam("detailsLevel", DetailsLevel.BASIC)
            .queryParam("paginationDetails", Toggle.ON)
            .build().toUriString()

        val method = when (criteria != null) {
            true -> HttpMethod.POST
            else -> HttpMethod.GET
        }

        return try {
            mambuRestTemplate.exchange<Any>(url, method, HttpEntity(criteria, headers)).headers.getPaginationDetails()
        } catch (e: Exception) {
            logger.error(e) { e.message }
            when (e is HttpStatusCodeException) {
                true -> throw MambuException("Status: ${e.statusCode}, Body: ${e.responseBodyAsString}", e)
                else -> throw MambuException(e.message ?: "", e)
            }
        }
    }

    private inline fun <reified L : List<T>, T> exchangeForAllResult(path: String, criteria: SearchCriteria? = null): List<T> {
        val total = exchangeForPaginationDetails(path, criteria).total
        val result = mutableListOf<T>()

        if (total != null) {
            for (offset in 0..total step mambuProperties.api.limit) {
                val page = exchangeForPagedResult<L>(path, criteria, offset)
                result += (page ?: emptyList())
            }
        }

        return result
    }

    private inline fun <reified T> exchangeForSingleResult(path: String, criteria: SearchCriteria? = null): T? {
        val url = UriComponentsBuilder
            .fromHttpUrl(mambuProperties.api.url)
            .path(path)
            .queryParam("detailsLevel", DetailsLevel.FULL)
            .queryParam("paginationDetails", Toggle.OFF)
            .build().toUriString()

        val method = when (criteria != null) {
            true -> HttpMethod.POST
            else -> HttpMethod.GET
        }

        return try {
            mambuRestTemplate.exchange(url, method, HttpEntity(criteria, headers), typeReference<T>()).body
        } catch (e: Exception) {
            logger.error(e) { e.message }
            when (e is HttpStatusCodeException) {
                true -> throw MambuException("Status: ${e.statusCode}, Body: ${e.responseBodyAsString}", e)
                else -> throw MambuException(e.message ?: "", e)
            }
        }
    }

    private inline fun <reified L> exchangeForPagedResult(path: String, criteria: SearchCriteria? = null, offset: Int = 0): L? {
        val url = UriComponentsBuilder
            .fromHttpUrl(mambuProperties.api.url)
            .path(path)
            .queryParam("detailsLevel", DetailsLevel.FULL)
            .queryParam("paginationDetails", Toggle.OFF)
            .queryParam("offset", offset)
            .queryParam("limit", mambuProperties.api.limit)
            .build().toUriString()

        val method = when (criteria != null) {
            true -> HttpMethod.POST
            else -> HttpMethod.GET
        }

        return try {
            mambuRestTemplate.exchange(url, method, HttpEntity(criteria, headers), typeReference<L>()).body
        } catch (e: Exception) {
            logger.error(e) { e.message }
            when (e is HttpStatusCodeException) {
                true -> throw MambuException("Status: ${e.statusCode}, Body: ${e.responseBodyAsString}", e)
                else -> throw MambuException(e.message ?: "", e)
            }
        }
    }

    fun getBranches(): List<Branch> {
        return exchangeForAllResult("/api/branches", null)
    }

    fun getBranchByEncodedKey(encodedKey: String): Branch? {
        return exchangeForSingleResult("/api/branches/$encodedKey")
    }

    fun getDepositProducts(): List<DepositProduct> {
        return exchangeForAllResult("/api/depositproducts")
    }

    fun searchCreditArrangementsAfter(date: LocalDate): List<CreditArrangement> {
        val criteria = SearchCriteria(
            SearchCriteria.FilterCriteria("expireDate", Operator.AFTER, date.toInstant().toIsoOffsetDateTime()),
            SearchCriteria.FilterCriteria("state", Operator.IN, listOf(CreditArrangement.State.ACTIVE.name, CreditArrangement.State.CLOSED.name)),
        )

        return exchangeForAllResult("/api/creditarrangements:search", criteria)
    }

    fun searchLoans(productTypeKey: String, fromDate: LocalDate, toDate: LocalDate, states: List<LoanAccountFullDetails.State>): List<LoanAccountFullDetails> {
        val criteria = SearchCriteria(
            SearchCriteria.FilterCriteria("creationDate", Operator.BETWEEN, fromDate.toInstant().toIsoOffsetDateTime(), toDate.toInstant().toIsoOffsetDateTime()),
            SearchCriteria.FilterCriteria("productTypeKey", Operator.EQUALS, productTypeKey),
            SearchCriteria.FilterCriteria("accountState", Operator.IN, states.map { it.name })
        )

        return exchangeForAllResult("/api/loans:search", criteria)
    }
}