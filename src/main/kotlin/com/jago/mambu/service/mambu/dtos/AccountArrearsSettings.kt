package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.Inclusion

data class AccountArrearsSettings(
    val encodedKey: String?,

    val dateCalculationMethod: DateCalculationMethod?,
    val nonWorkingDaysMethod: Inclusion?,
    val monthlyToleranceDay: Int?,

    val toleranceCalculationMethod: ToleranceCalculationMethod?,
    val toleranceAmount: Double?,
    val tolerancePercentageOfOutstandingPrincipal: Double?,
    val tolerancePeriod: Int?,
) {

    enum class DateCalculationMethod {
        ACCOUNT_FIRST_WENT_TO_ARREARS,
        LAST_LATE_REPAYMENT,
        ACCOUNT_FIRST_BREACHED_MATERIALITY_THRESHOLD,
    }

    enum class ToleranceCalculationMethod {
        ARREARS_TOLERANCE_PERIOD,
        MONTHLY_ARREARS_TOLERANCE_DAY,
    }
}
