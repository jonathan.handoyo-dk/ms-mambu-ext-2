package com.jago.mambu.service.mambu.dtos

data class Address(
    val encodedKey: String?,
    val parentKey: String?,

    val city: String?,
    val postcode: String?,
    val region: String?,
    val country: String?,
    val line1: String?,
    val line2: String?,

    val latitude: Int?,
    val longitude: Int?,

    val indexInList: Int?,
)
