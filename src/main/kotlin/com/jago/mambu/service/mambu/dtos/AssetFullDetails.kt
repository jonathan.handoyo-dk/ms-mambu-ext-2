package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.HolderType

data class AssetFullDetails(
    val encodedKey: String?,
    val assetName: String?,
    val depositAccountKey: String?,
    val amount: Double?,
    val originalAmount: Double?,
    val originalCurrency: Currency?,

    val guarantorKey: String?,
    val guarantorType: HolderType?,
)
