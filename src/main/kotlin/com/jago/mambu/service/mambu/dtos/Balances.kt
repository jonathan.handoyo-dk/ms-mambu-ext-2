package com.jago.mambu.service.mambu.dtos

data class Balances(
    val redrawBalance: Double?,
    val holdBalance: Double?,

    val principalDue: Double?,
    val principalPaid: Double?,
    val principalBalance: Double?,

    val interestDue: Double?,
    val interestPaid: Double?,
    val interestBalance: Double?,

    val interestFromArrearsDue: Double?,
    val interestFromArrearsPaid: Double?,
    val interestFromArrearsBalance: Double?,

    val feesDue: Double?,
    val feesPaid: Double?,
    val feesBalance: Double?,

    val penaltyDue: Double?,
    val penaltyPaid: Double?,
    val penaltyBalance: Double?,
)
