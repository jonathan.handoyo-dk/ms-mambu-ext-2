package com.jago.mambu.service.mambu.dtos

data class BillingCycleDays(
    val days: List<Int>?,
)