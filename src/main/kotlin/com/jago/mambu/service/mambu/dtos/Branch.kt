package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.State
import java.time.Instant

data class Branch(
    val encodedKey: String?,
    val id: String?,
    val name: String?,
    val emailAddress: String?,
    val phoneNumber: String?,
    val state: State?,

    val addresses: List<Address>?,
    val branchHolidays: List<Holiday>?,
    val notes: String?,

    val creationDate: Instant?,
    val lastModifiedDate: Instant?,
)
