package com.jago.mambu.service.mambu.dtos

data class BranchSettings(
    val availableProductBranches: List<String>?,
    val forAllBranches: Boolean?,
)
