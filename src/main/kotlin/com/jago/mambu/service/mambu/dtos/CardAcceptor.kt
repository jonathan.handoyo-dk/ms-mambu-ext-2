package com.jago.mambu.service.mambu.dtos

data class CardAcceptor(
    val name: String?,

    val street: String?,
    val city: String?,
    val state: String?,
    val zip: String?,
    val country: String?,

    val mcc: Int?,
)
