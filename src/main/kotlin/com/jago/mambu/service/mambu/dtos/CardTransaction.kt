package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.CurrencyCode
import java.time.Instant

data class CardTransaction(
    val encodedKey: String?,
    val externalAuthorizationReferenceId: String?,
    val externalReferenceId: String?,

    val advice: Boolean?,
    val amount: Double?,
    val currencyCode: CurrencyCode?,

    val cardAcceptor: CardAcceptor?,
    val cardToken: String?,

    val userTransactionTime: Instant?,
)
