package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.ExposureLimitType
import com.jago.mambu.service.mambu.enums.HolderType
import java.time.Instant

data class CreditArrangement(
    val encodedKey: String?,
    val id: String?,

    val amount: Double?,
    val availableCreditAmount: Double?,
    val consumedCreditAmount: Double?,
    val exposureLimitType: ExposureLimitType?,
    val holderKey: String?,
    val holderType: HolderType?,
    val currency: Currency?,

    val state: State?,
    val subState: SubState?,

    val notes: String?,

    val approvedDate: Instant?,
    val closedDate: Instant?,
    val creationDate: Instant?,
    val expireDate: Instant?,
    val startDate: Instant?,
    val lastModifiedDate: Instant?,
) {

    enum class State {
        PENDING_APPROVAL,
        APPROVED,
        ACTIVE,
        CLOSED,
        WITHDRAWN,
        REJECTED,
    }

    enum class SubState {
        PENDING_APPROVAL,
        APPROVED,
        ACTIVE,
        CLOSED,
        WITHDRAWN,
        REJECTED,
    }
}