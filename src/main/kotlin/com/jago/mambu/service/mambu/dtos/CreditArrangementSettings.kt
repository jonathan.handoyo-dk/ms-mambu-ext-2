package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.Requirement

data class CreditArrangementSettings(
    val creditArrangementRequirement: Requirement?,
)
