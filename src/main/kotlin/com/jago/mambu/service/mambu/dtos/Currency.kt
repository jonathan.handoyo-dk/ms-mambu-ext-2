package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.CurrencyCode

data class Currency(
    val code: CurrencyCode,
    val currencyCode: String,
)
