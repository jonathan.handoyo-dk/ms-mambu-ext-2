package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.CustomPaymentAmountType

data class CustomPaymentAmount(
    val amount: Double?,
    val customPaymentAmountType: CustomPaymentAmountType?,
    val predefinedFeeKey: String?,
    val taxOnAmount: Double?,
)
