package com.jago.mambu.service.mambu.dtos

data class CustomPredefinedFee(
    val encodedKey: String?,
    val amount: Double?,
    val predefinedFeeEncodedKey: String?,
)
