package com.jago.mambu.service.mambu.dtos

data class DecimalInterval(
    val defaultValue: Double?,
    val minValue: Double?,
    val maxValue: Double?,
)
