package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.FinancialResult

data class DepositGlAccountingRule(
    val encodedKey: String?,
    val financialResource: FinancialResult?,
    val glAccountKey: String?,
)
