package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.PeriodUnit

data class DepositMaturitySettings(
    val maturityPeriod: IntegerInterval?,
    val maturityPeriodUnit: PeriodUnit?,
)
