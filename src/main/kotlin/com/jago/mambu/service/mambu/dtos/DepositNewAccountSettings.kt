package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.IdGeneratorType

data class DepositNewAccountSettings(
    val idGeneratorType: IdGeneratorType?,
    val idPattern: String?,
)
