package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.State
import java.time.Instant

data class DepositProduct(
    val encodedKey: String?,
    val id: String?,
    val category: Category?,
    val name: String?,
    val type: Type?,

    val accountingSettings: DepositProductAccountingSettings?,
    val availabilitySettings: DepositProductAvailabilitySettings?,
    val creditArrangementSettings: CreditArrangementSettings?,
    val currencySettings: DepositProductCurrencySettings?,
    val feesSettings: DepositProductFeeSettings?,
    val interestSettings: DepositProductInterestSettings?,
    val maturitySettings: DepositMaturitySettings?,
    val newAccountSettings: DepositNewAccountSettings?,
    val offsetSettings: DepositProductOffsetSettings?,
    val overdraftInterestSettings: OverdraftInterestSettings?,
    val overdraftSettings: DepositProductOverdraftSettings?,
    val taxSettings: DepositProductTaxSettings?,

    val templates: List<DocumentTemplate>?,
    val internalControls: DepositProductInternalControls?,
    val notes: String?,
    val state: State?,

    val creationDate: Instant?,
    val lastModifiedDate: Instant?,

) {

    enum class Category {
        PERSONAL_DEPOSIT,
        BUSINESS_DEPOSIT,
        DAILY_BANKING_ACCOUNTS,
        BUSINESS_BANKING_ACCOUNTS,
        STORED_VALUE_ACCOUNTS,
        UNCATEGORIZED,
    }

    enum class Type {
        CURRENT_ACCOUNT,
        REGULAR_SAVINGS,
        FIXED_DEPOSIT,
        SAVINGS_PLAN,
        INVESTOR_ACCOUNT,
    }
}
