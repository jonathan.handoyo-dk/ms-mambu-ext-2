package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.AccountingMethod
import com.jago.mambu.service.mambu.enums.InterestAccruedAccountingMethod

data class DepositProductAccountingSettings(
    val accountingMethod: AccountingMethod?,
    val accountingRules: List<DepositGlAccountingRule>?,
    val interestAccruedAccountingMethod: InterestAccruedAccountingMethod?,
)
