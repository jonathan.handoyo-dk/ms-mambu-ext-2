package com.jago.mambu.service.mambu.dtos

data class DepositProductAvailabilitySettings(
    val availableFor: List<String>?,
    val branchSettings: BranchSettings?,
)
