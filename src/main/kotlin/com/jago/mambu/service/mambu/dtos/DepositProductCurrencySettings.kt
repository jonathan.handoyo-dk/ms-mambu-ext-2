package com.jago.mambu.service.mambu.dtos

data class DepositProductCurrencySettings(
    val currencies: List<Currency>?,
)
