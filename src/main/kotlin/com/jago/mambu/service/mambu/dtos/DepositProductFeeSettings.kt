package com.jago.mambu.service.mambu.dtos

data class DepositProductFeeSettings(
    val allowArbitraryFees: Boolean?,
    val fees: List<DepositProductPredefinedFee>?,
)
