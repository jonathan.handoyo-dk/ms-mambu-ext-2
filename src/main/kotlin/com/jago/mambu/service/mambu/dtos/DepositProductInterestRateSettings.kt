package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.InterestChargeFrequency
import com.jago.mambu.service.mambu.enums.InterestRateSource
import com.jago.mambu.service.mambu.enums.InterestRateTerms
import com.jago.mambu.service.mambu.enums.PeriodUnit

data class DepositProductInterestRateSettings(
    val encodedKey: String?,
    val indexSourceKey: String?,

    val accrueInterestAfterMaturity: Boolean?,
    val allowNegativeInterestRate: Boolean?,

    val interestChargeFrequency: InterestChargeFrequency?,
    val interestChargeFrequencyCount: Int?,

    val interestRate: DecimalInterval?,
    val interestRateReviewCount: Int?,
    val interestRateReviewUnit: PeriodUnit?,
    val interestRateSource: InterestRateSource?,
    val interestRateTerms: InterestRateTerms?,
    val interestRateTiers: List<DepositProductInterestRateTier>?,
)
