package com.jago.mambu.service.mambu.dtos

data class DepositProductInterestRateTier(
    val encodedKey: String?,
    val endingBalance: Double?,
    val endingDay: Int?,
    val interestRate: Double?,
)
