package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.DaysInYear
import com.jago.mambu.service.mambu.enums.InterestCalculationBalance

data class DepositProductInterestSettings(
    val collectInterestWhenLocked: Boolean?,
    val daysInYear: DaysInYear?,

    val interestCalculationBalance: InterestCalculationBalance?,
    val interestPaidIntoAccount: Boolean?,
    val interestPaymentSettings: InterestPaymentSettings?,
    val interestRateSettings: DepositProductInterestRateSettings?,
    val maximumBalance: Double?,
)
