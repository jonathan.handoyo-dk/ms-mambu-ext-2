package com.jago.mambu.service.mambu.dtos

data class DepositProductInternalControls(
    val dormancyPeriodDays: Int?,
    val maxWithdrawalAmount: Double?,
    val openingBalance: DecimalInterval?,
    val recommendedDepositAmount: Double?,
)
