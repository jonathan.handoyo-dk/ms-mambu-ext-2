package com.jago.mambu.service.mambu.dtos

data class DepositProductOffsetSettings(
    val allowOffset: Boolean?,
)
