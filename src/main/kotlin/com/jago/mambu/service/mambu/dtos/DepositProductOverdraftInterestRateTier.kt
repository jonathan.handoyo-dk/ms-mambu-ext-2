package com.jago.mambu.service.mambu.dtos

data class DepositProductOverdraftInterestRateTier(
    val encodedKey: String?,
    val endingBalance: Double?,
    val interestRate: Double?,
)
