package com.jago.mambu.service.mambu.dtos

data class DepositProductOverdraftSettings(
    val allowOverdraft: Boolean?,
    val allowTechnicalOverdraft: Boolean?,
    val maxOverdraftLimit: Double?,
)
