package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.*
import java.time.Instant

data class DepositProductPredefinedFee(
    val encodedKey: String?,
    val id: String?,

    val accountingRules: List<DepositGlAccountingRule>?,

    val name: String?,
    val amount: Double?,
    val state: State?,
    val trigger: Trigger?,
    val feeApplication: Requirement?,

    val amountCalculationMethod: AmountCalculationMethod?,
    val applyDateMethod: ApplyDateMethod?,

    val creationDate: Instant?,
    val lastModifiedDate: Instant?,
)