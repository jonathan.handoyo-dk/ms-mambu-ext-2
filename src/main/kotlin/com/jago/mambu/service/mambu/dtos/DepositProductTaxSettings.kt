package com.jago.mambu.service.mambu.dtos

data class DepositProductTaxSettings(
    val withholdingTaxEnabled: Boolean?,
)
