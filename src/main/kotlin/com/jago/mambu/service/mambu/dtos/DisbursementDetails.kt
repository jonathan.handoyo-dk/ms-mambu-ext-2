package com.jago.mambu.service.mambu.dtos

import java.time.Instant

data class DisbursementDetails(
    val encodedKey: String?,

    val disbursementDate: Instant?,
    val expectedDisbursementDate: Instant?,
    val firstRepaymentDate: Instant?,

    val fees: List<CustomPredefinedFee>?,
    val transactionDetails: LoanTransactionDetails?,
)
