package com.jago.mambu.service.mambu.dtos

import java.time.Instant

data class DocumentTemplate(
    val encodedKey: String?,

    val name: String?,
    val type: Type?,

    val creationDate: Instant?,
    val lastModifiedDate: Instant?,
) {

    enum class Type {
        ACCOUNT,
        TRANSACTION,
        ACCOUNT_WITH_TRANSACTIONS,
    }
}
