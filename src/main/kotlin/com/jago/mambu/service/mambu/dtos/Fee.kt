package com.jago.mambu.service.mambu.dtos

data class Fee(
    val amount: Double?,
    val name: String?,
    val predefinedFeeKey: String?,
    val taxAmount: Double?,
    val trigger: Trigger?,
) {

    enum class Trigger {
        MANUAL,
        MANUAL_PLANNED,
        DISBURSEMENT,
        CAPITALIZED_DISBURSEMENT,
        UPFRONT_DISBURSEMENT,
        LATE_REPAYMENT,
        PAYMENT_DUE,
        PAYMENT_DUE_APPLIED_ON_DUE_DATES,
        ARBITRARY,
        IOF,
    }
}
