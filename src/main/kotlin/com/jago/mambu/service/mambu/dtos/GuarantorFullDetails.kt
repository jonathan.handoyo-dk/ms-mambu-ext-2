package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.HolderType

data class GuarantorFullDetails(
    val encodedKey: String?,

    val guarantorKey: String?,
    val guarantorType: HolderType?,

    val amount: Double?,
    val assetName: String?,
    val depositAccountKey: String?,
)
