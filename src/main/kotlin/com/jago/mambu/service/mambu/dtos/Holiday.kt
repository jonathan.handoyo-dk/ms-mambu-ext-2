package com.jago.mambu.service.mambu.dtos

import java.time.Instant
import java.time.LocalDate

data class Holiday(
    val encodedKey: String?,
    val id: Long?,
    val date: LocalDate?,
    val name: String?,
    val isAnnuallyRecurring: Boolean?,
    val creationDate: Instant?,
)
