package com.jago.mambu.service.mambu.dtos

data class IntegerInterval(
    val defaultValue: Int?,
    val minValue: Int?,
    val maxValue: Int?,
)
