package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.InterestPaymentPoint

data class InterestPaymentSettings(
    val interestPaymentDates: List<MonthAndDay>?,
    val interestPaymentPoint: InterestPaymentPoint?,
)
