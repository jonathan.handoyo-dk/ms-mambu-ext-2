package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.*

data class InterestSettings(
    val accountInterestRateSettings: Any?,
    val accrueInterestAfterMaturity: Boolean?,
    val accrueLateInterest: Boolean?,

    val interestApplicationMethod: InterestApplicationMethod?,
    val interestBalanceCalculationMethod: InterestBalanceCalculationMethod?,
    val interestCalculationMethod: InterestCalculationMethod?,
    val interestChargeFrequency: InterestChargeFrequency?,

    val interestRate: Double?,
    val interestRateReviewCount: Int?,
    val interestRateReviewUnit: PeriodUnit?,
    val interestRateSource: InterestRateSource?,
    val interestSpread: Double?,
    val interestType: InterestType?,
)
