package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.HolderType

data class InvestorFund(
    val encodedKey: String?,
    val id: String?,

    val amount: Double?,
    val interestCommission: Double?,
    val sharePercentage: Double?,

    val assetName: String?,
    val depositAccountKey: String?,
    val guarantorKey: String?,
    val guarantorType: HolderType?,
)
