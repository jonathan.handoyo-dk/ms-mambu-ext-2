package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.FuturePaymentsAcceptance
import com.jago.mambu.service.mambu.enums.HolderType
import com.jago.mambu.service.mambu.enums.LatePaymentsRecalculationMethod
import com.jago.mambu.service.mambu.enums.PaymentMethod
import java.time.Instant

data class LoanAccountFullDetails(
    val encodedKey: String?,
    val id: String?,

    val accountHolderKey: String?,
    val accountHolderType: HolderType?,
    val accountState: State?,
    val accountSubState: SubState?,
    val productTypeKey: String?,

    val loanAmount: Double?,
    val loanName: String?,
    val accruedInterest: Double?,
    val accruedPenalty: Double?,

    val balances: Balances?,
    val currency: Currency?,

    val daysInArrears: Int?,
    val daysLate: Int?,

    val interestAccruedInBillingCycle: Double?,
    val interestCommission: Double?,
    val interestFromArrearsAccrued: Double?,

    val arrearsTolerancePeriod: Int?,
    val assets: List<AssetFullDetails>?,
    val fundingSources: List<InvestorFund>?,
    val guarantors: List<GuarantorFullDetails>?,

    val activationTransactionKey: String?,
    val assignedBranchKey: String?,
    val assignedCentreKey: String?,
    val assignedUserKey: String?,
    val creditArrangementKey: String?,
    val migrationEventKey: String?,
    val originalAccountKey: String?,
    val rescheduledAccountKey: String?,
    val settlementAccountKey: String?,

    val allowOffset: Boolean?,
    val modifyInterestForFirstInstallment: Boolean?,
    val futurePaymentsAcceptance: FuturePaymentsAcceptance?,
    val paymentMethod: PaymentMethod?,
    val paymentHolidaysAccruedInterest: Double?,

    val disbursementDetails: DisbursementDetails?,

    val accountArrearsSettings: AccountArrearsSettings?,
    val interestSettings: InterestSettings?,
    val penaltySettings: PenaltySettings?,
    val prepaymentSettings: PrepaymentSettings?,
    val principalPaymentSettings: PrincipalPaymentAccountSettings?,
    val redrawSettings: LoanAccountRedrawSettings?,
    val scheduleSettings: ScheduleSettings?,

    val plannedInstallmentFees: List<PlannedInstallmentFee>?,

    val notes: String?,
    val taxRate: Double?,
    val tranches: List<LoanTranche>,

    val approvedDate: Instant?,
    val closedDate: Instant?,
    val creationDate: Instant?,
    val lastAccountAppraisalDate: Instant?,
    val lastInterestAppliedDate: Instant?,
    val lastInterestReviewDate: Instant?,
    val lastLockedDate: Instant?,
    val lastModifiedDate: Instant?,
    val lastSetToArrearsDate: Instant?,
    val lastTaxRateReviewDate: Instant?,
    val terminationDate: Instant?,

    val latePaymentsRecalculationMethod: LatePaymentsRecalculationMethod?,
    val lockedOperations: List<String>?, // <- TODO: enum!
) {

    enum class State {
        PARTIAL_APPLICATION,
        PENDING_APPROVAL,
        APPROVED,
        ACTIVE,
        ACTIVE_IN_ARREARS,
        CLOSED,
    }

    enum class SubState {
        PARTIALLY_DISBURSED,
        LOCKED,
        LOCKED_CAPPING,
        REFINANCED,
        RESCHEDULED,
        WITHDRAWN,
        REPAID,
        REJECTED,
        WRITTEN_OFF,
        TERMINATED,
    }
}
