package com.jago.mambu.service.mambu.dtos

data class LoanAccountRedrawSettings(
    val restrictNextDueWithdrawal: Boolean?,
)
