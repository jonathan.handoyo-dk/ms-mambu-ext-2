package com.jago.mambu.service.mambu.dtos

data class LoanAffectedAmounts(
    val deferredInterestAmount: Double?,
    val feesAmount: Double?,
    val fundersInterestAmount: Double?,
    val interestAmount: Double?,
    val interestFromArrearsAmount: Double?,
    val organizationCommissionAmount: Double?,
    val paymentHolidaysInterestAmount: Double?,
    val penaltyAmount: Double?,
    val principalAmount: Double?,
)
