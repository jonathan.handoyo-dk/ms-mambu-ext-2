package com.jago.mambu.service.mambu.dtos

data class LoanTerms(
    val interestSettings: TransactionInterestSettings?,
    val periodicPayment: Double?,
    val principalPaymentAmount: Double?,
    val principalPaymentPercentage: Double?,
)
