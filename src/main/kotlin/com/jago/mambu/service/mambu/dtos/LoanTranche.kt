package com.jago.mambu.service.mambu.dtos

data class LoanTranche(
    val encodedKey: String?,
    val amount: Double?,
    val disbursementDetails: TrancheDisbursementDetails?,
    val fees: List<CustomPredefinedFee>?,
    val trancheNumber: Int?,
)
