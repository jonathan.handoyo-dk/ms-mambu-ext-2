package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.CurrencyCode
import com.jago.mambu.service.mambu.enums.PrepaymentRecalculationMethod
import java.time.Instant

data class LoanTransaction(
    val encodedKey: String?,
    val adjustmentTransactionKey: String?,
    val branchKey: String?,
    val centreKey: String?,
    val installmentEncodedKey: String?,
    val migrationEventKey: String?,
    val originalTransactionKey: String?,
    val parentAccountKey: String?,
    val parentLoanTransactionKey: String?,
    val tillKey: String?,
    val userKey: String?,

    val prepaymentRecalculationMethod: PrepaymentRecalculationMethod?,

    val id: String?,
    val externalId: String?,
    val type: Type?,

    val amount: Double?,
    val originalAmount: Double?,
    val currency: Currency?,
    val originalCurrencyCode: CurrencyCode?,
    val fees: List<Fee>?,
    val taxes: Taxes?,
    val terms: LoanTerms?,
    val notes: String?,

    val transactionDetails: TransactionDetails?,
    val transferDetails: TransferDetails?,

    val affectedAmounts: LoanAffectedAmounts?,
    val customPaymentAmounts: List<CustomPaymentAmount>?,
    val accountBalances: TransactionBalances?,

    val cardTransaction: CardTransaction?,

    val bookingDate: Instant?,
    val creationDate: Instant?,
    val valueDate: Instant?,
) {

    enum class Type {
        IMPORT,
        DISBURSEMENT,
        DISBURSEMENT_ADJUSTMENT,
        WRITE_OFF,
        WRITE_OFF_ADJUSTMENT,
        REPAYMENT,
        PAYMENT_MADE,
        WITHDRAWAL_REDRAW,
        WITHDRAWAL_REDRAW_ADJUSTMENT,
        FEE_APPLIED,
        FEE_CHARGED,
        FEES_DUE_REDUCED,
        FEE_ADJUSTMENT,
        PENALTY_APPLIED,
        PENALTY_ADJUSTMENT,
        PENALTIES_DUE_REDUCED,
        REPAYMENT_ADJUSTMENT,
        PAYMENT_MADE_ADJUSTMENT,
        INTEREST_RATE_CHANGED,
        TAX_RATE_CHANGED,
        PENALTY_RATE_CHANGED,
        INTEREST_APPLIED,
        INTEREST_APPLIED_ADJUSTMENT,
        INTEREST_DUE_REDUCED,
        PENALTY_REDUCTION_ADJUSTMENT,
        FEE_REDUCTION_ADJUSTMENT,
        INTEREST_REDUCTION_ADJUSTMENT,
        DEFERRED_INTEREST_APPLIED,
        DEFERRED_INTEREST_APPLIED_ADJUSTMENT,
        DEFERRED_INTEREST_PAID,
        DEFERRED_INTEREST_PAID_ADJUSTMENT,
        INTEREST_LOCKED,
        FEE_LOCKED,
        PENALTY_LOCKED,
        INTEREST_UNLOCKED,
        FEE_UNLOCKED,
        PENALTY_UNLOCKED,
        REDRAW_TRANSFER,
        REDRAW_REPAYMENT,
        REDRAW_TRANSFER_ADJUSTMENT,
        REDRAW_REPAYMENT_ADJUSTMENT,
        TRANSFER,
        TRANSFER_ADJUSTMENT,
        BRANCH_CHANGED,
        TERMS_CHANGED,
        CARD_TRANSACTION_REVERSAL,
        CARD_TRANSACTION_REVERSAL_ADJUSTMENT,
        DUE_DATE_CHANGED,
        DUE_DATE_CHANGED_ADJUSTMENT,
        ACCOUNT_TERMINATED,
        ACCOUNT_TERMINATED_ADJUSTMENT,
    }
}
