package com.jago.mambu.service.mambu.dtos

data class LoanTransactionDetails(
    val encodedKey: String?,
    val internalTransfer: Boolean?,
    val targetDepositAccountKey: String?,
    val transactionChannelId: String?,
    val transactionChannelKey: String?,
)