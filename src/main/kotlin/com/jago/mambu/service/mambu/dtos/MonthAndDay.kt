package com.jago.mambu.service.mambu.dtos

data class MonthAndDay(
    val day: Int?,
    val month: Int?,
)
