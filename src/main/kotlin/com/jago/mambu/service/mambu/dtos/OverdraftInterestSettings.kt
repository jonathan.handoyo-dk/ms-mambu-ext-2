package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.DaysInYear
import com.jago.mambu.service.mambu.enums.InterestCalculationBalance

data class OverdraftInterestSettings(
    val daysInYear: DaysInYear?,
    val interestCalculationBalance: InterestCalculationBalance?,
    val interestRateSettings: DepositProductOverdraftInterestRateSettings?,
)
