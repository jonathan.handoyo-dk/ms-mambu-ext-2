package com.jago.mambu.service.mambu.dtos

data class PenaltySettings(
    val loanPenaltyCalculationMethod: String?, // <- TODO: enum!
    val penaltyRate: Double?,
)
