package com.jago.mambu.service.mambu.dtos

data class PeriodicPayment(
    val encodedKey: String?,
    val amount: Double?,
    val toInstallment: Int?,
)
