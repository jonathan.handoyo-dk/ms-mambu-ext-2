package com.jago.mambu.service.mambu.dtos

import java.time.Instant

data class PlannedInstallmentFee(
    val encodedKey: String?,
    val installmentKey: String?,
    val installmentNumber: Int?,
    val predefinedFeeKey: String?,
    val amount: Double?,
    val applyOnDate: Instant?,
)