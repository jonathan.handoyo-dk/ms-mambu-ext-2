package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.ApplyInterestOnPrepaymentMethod
import com.jago.mambu.service.mambu.enums.ElementsRecalculationMethod
import com.jago.mambu.service.mambu.enums.PrepaymentRecalculationMethod
import com.jago.mambu.service.mambu.enums.PrincipalPaidInstallmentStatus

data class PrepaymentSettings(
    val applyInterestOnPrepaymentMethod: ApplyInterestOnPrepaymentMethod?,
    val elementsRecalculationMethod: ElementsRecalculationMethod?,
    val prepaymentRecalculationMethod: PrepaymentRecalculationMethod?,
    val principalPaidInstallmentStatus: PrincipalPaidInstallmentStatus?,
)
