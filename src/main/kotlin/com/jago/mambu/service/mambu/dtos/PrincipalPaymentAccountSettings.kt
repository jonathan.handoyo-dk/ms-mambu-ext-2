package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.PrincipalPaymentMethod
import com.jago.mambu.service.mambu.enums.TotalDuePayment

data class PrincipalPaymentAccountSettings(
    val encodedKey: String?,

    val amount: Double?,
    val percentage: Double?,

    val principalCeilingValue: Double?,
    val principalFloorValue: Double?,
    val totalDueAmountFloor: Double?,

    val principalPaymentMethod: PrincipalPaymentMethod?,
    val totalDuePayment: TotalDuePayment?,

    val includeFeesInFloorAmount: Boolean?,
    val includeInterestInFloorAmount: Boolean?,
)