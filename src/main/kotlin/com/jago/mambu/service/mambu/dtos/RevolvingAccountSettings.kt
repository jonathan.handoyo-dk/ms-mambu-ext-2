package com.jago.mambu.service.mambu.dtos

data class RevolvingAccountSettings(
    val numberOfPreviewedInstalments: Int?,
)
