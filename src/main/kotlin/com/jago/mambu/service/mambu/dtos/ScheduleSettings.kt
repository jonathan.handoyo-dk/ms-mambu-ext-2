package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.*

data class ScheduleSettings(
    val billingCycle: BillingCycleDays?,
    val defaultFirstRepaymentDueDateOffset: Int?,
    val fixedDaysOfMonth: List<Int>?,
    val hasCustomSchedule: Boolean?,

    val gracePeriod: Int?,
    val gracePeriodType: GracePeriodType?,

    val paymentPlan: List<PeriodicPayment>?,
    val periodicPayment: Double?,
    val previewSchedule: RevolvingAccountSettings?,

    val principalRepaymentInterval: Int?,
    val repaymentInstallments: Int?,
    val repaymentPeriodCount: Int?,

    val repaymentPeriodUnit: PeriodUnit?,
    val repaymentScheduleMethod: RepaymentScheduleMethod?,
    val scheduleDueDatesMethod: ScheduleDueDatesMethod?,
    val shortMonthHandlingMethod: ShortMonthHandlingMethod?,
)
