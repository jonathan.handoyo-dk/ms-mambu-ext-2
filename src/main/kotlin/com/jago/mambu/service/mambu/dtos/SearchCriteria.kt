package com.jago.mambu.service.mambu.dtos

import com.jago.mambu.service.mambu.enums.Operator
import com.jago.mambu.service.mambu.enums.Order

data class SearchCriteria(
    val filterCriteria: List<FilterCriteria>?,
    val sortingCriteria: SortingCriteria?,
) {
    constructor(vararg filterCriterias: FilterCriteria): this(listOf(*filterCriterias), null)

    data class FilterCriteria(
        val field: String,
        val operator: Operator,
        val value: String? = null,
        val secondValue: String? = null,
        val values: List<String>? = null,
    ) {
        constructor(field: String, operator: Operator, value: String): this(field, operator, value, null, null)
        constructor(field: String, operator: Operator, value: String, secondValue: String): this(field, operator, value, secondValue, null)
        constructor(field: String, operator: Operator, values: List<String>): this(field, operator, null, null, values)
    }

    data class SortingCriteria(
        val field: String,
        val order: Order,
    )
}