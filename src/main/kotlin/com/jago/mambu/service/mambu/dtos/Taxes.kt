package com.jago.mambu.service.mambu.dtos

data class Taxes(
    val deferredTaxOnInterestAmount: Double?,
    val taxOnFeesAmount: Double?,
    val taxOnInterestAmount: Double?,
    val taxOnInterestFromArrearsAmount: Double?,
    val taxOnPaymentHolidaysInterest: Double?,
    val taxOnPenaltyAmount: Double?,
    val taxRate: Double?,
)
