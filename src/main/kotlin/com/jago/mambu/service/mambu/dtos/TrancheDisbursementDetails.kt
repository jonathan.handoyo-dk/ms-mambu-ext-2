package com.jago.mambu.service.mambu.dtos

import java.time.Instant

data class TrancheDisbursementDetails(
    val disbursementTransactionKey: String?,
    val expectedDisbursementDate: Instant?,
)
