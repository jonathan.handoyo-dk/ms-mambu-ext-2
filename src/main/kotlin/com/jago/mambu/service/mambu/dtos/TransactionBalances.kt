package com.jago.mambu.service.mambu.dtos

data class TransactionBalances(
    val advancePosition: Double?,
    val arrearsPosition: Double?,
    val expectedPrincipalRedraw: Double?,
    val principalBalance: Double?,
    val redrawBalance: Double?,
    val totalBalance: Double?,
)
