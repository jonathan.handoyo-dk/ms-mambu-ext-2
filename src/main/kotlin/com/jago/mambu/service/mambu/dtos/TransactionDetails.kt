package com.jago.mambu.service.mambu.dtos

data class TransactionDetails(
    val transactionChannelId: String?,
    val transactionChannelKey: String?,
)
