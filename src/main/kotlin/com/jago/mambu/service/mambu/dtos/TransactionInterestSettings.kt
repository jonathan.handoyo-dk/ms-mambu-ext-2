package com.jago.mambu.service.mambu.dtos

data class TransactionInterestSettings(
    val indexInterestRate: Double?,
    val interestRate: Double?,
)
