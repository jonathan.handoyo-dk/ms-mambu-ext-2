package com.jago.mambu.service.mambu.dtos

data class TransferDetails(
    val linkedDepositTransactionKey: String?,
    val linkedLoanTransactionKey: String?,
)
