package com.jago.mambu.service.mambu.enums

enum class AccountingMethod {
    NONE,
    CASH,
    ACCRUAL,
}