package com.jago.mambu.service.mambu.enums

enum class ApplyDateMethod {
    MONTHLY_FROM_ACTIVATION,
    FIRST_OF_EVERY_MONTH,
}