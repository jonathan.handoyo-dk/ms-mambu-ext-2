package com.jago.mambu.service.mambu.enums

enum class ApplyInterestOnPrepaymentMethod {
    AUTOMATIC,
    MANUAL,
}