package com.jago.mambu.service.mambu.enums

enum class CustomPaymentAmountType {
    PRINCIPAL,
    INTEREST,
    MANUAL_FEE,
    UPFRONT_DISBURSEMENT_FEE,
    LATE_REPAYMENT_FEE,
    PAYMENT_DUE_FEE,
    PENALTY,
}