package com.jago.mambu.service.mambu.enums

enum class DaysInYear {
    ACTUAL_365_FIXED,
    ACTUAL_360,
    ACTUAL_ACTUAL_ISDA,
    E30_360,
    E30_42_365,
    BUS_252,
}