package com.jago.mambu.service.mambu.enums

enum class DetailsLevel {
    BASIC,
    FULL,
}