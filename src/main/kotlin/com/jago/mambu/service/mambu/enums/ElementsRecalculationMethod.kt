package com.jago.mambu.service.mambu.enums

enum class ElementsRecalculationMethod {
    PRINCIPAL_EXPECTED_FIXED,
    TOTAL_EXPECTED_FIXED,
}