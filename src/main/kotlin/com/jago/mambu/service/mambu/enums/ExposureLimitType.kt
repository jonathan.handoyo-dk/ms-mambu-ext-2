package com.jago.mambu.service.mambu.enums

enum class ExposureLimitType {
    APPROVED_AMOUNT,
    OUTSTANDING_AMOUNT
}