package com.jago.mambu.service.mambu.enums

enum class FuturePaymentsAcceptance {
    NO_FUTURE_PAYMENTS,
    ACCEPT_FUTURE_PAYMENTS,
    ACCEPT_OVERPAYMENTS,
}