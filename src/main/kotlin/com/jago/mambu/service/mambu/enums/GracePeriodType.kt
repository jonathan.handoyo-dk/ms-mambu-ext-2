package com.jago.mambu.service.mambu.enums

enum class GracePeriodType {
    NONE,
    PAY_INTEREST_ONLY,
    INTEREST_FORGIVENESS,
}