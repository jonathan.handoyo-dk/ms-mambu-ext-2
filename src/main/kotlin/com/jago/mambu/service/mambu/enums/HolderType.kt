package com.jago.mambu.service.mambu.enums

enum class HolderType {
    CLIENT,
    GROUP
}