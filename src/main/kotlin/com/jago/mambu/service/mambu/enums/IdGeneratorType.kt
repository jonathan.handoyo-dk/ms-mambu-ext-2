package com.jago.mambu.service.mambu.enums

enum class IdGeneratorType {
    INCREMENTAL_NUMBER,
    RANDOM_PATTERN,
}