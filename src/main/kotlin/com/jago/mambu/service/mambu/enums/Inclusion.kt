package com.jago.mambu.service.mambu.enums

enum class Inclusion {
    INCLUDED,
    EXCLUDED,
}