package com.jago.mambu.service.mambu.enums

enum class InterestAccruedAccountingMethod {
    NONE,
    DAILY,
    END_OF_MONTH,
}