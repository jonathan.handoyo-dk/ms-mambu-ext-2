package com.jago.mambu.service.mambu.enums

enum class InterestApplicationMethod {
    AFTER_DISBURSEMENT,
    REPAYMENT_DUE_DATE,
}