package com.jago.mambu.service.mambu.enums

enum class InterestBalanceCalculationMethod {
    ONLY_PRINCIPAL,
    PRINCIPAL_AND_INTEREST,
}