package com.jago.mambu.service.mambu.enums

enum class InterestCalculationBalance {
    MINIMUM,
    AVERAGE,
    END_OF_DAY,
    MINIMUM_TO_END_OF_DAY
}