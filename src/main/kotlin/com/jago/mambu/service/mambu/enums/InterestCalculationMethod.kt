package com.jago.mambu.service.mambu.enums

enum class InterestCalculationMethod {
    FLAT,
    DECLINING_BALANCE,
    DECLINING_BALANCE_DISCOUNTED,
}