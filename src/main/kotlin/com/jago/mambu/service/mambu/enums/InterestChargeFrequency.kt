package com.jago.mambu.service.mambu.enums

enum class InterestChargeFrequency {
    ANNUALIZED,
    EVERY_MONTH,
    EVERY_FOUR_WEEKS,
    EVERY_WEEK,
    EVERY_DAY,
    EVERY_X_DAYS,
}