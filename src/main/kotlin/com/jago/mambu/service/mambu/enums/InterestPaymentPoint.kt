package com.jago.mambu.service.mambu.enums

enum class InterestPaymentPoint {
    FIRST_DAY_OF_MONTH,
    EVERY_WEEK,
    EVERY_OTHER_WEEK,
    EVERY_MONTH,
    EVERY_3_MONTHS,
    ON_FIXED_DATES,
    DAILY,
    ANNUALLY,
    BI_ANNUALLY,
    ON_ACCOUNT_MATURITY,
}