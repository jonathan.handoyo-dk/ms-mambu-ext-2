package com.jago.mambu.service.mambu.enums

enum class InterestRateSource {
    FIXED_INTEREST_RATE,
    INDEX_INTEREST_RATE,
}