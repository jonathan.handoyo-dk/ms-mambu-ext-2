package com.jago.mambu.service.mambu.enums

enum class InterestRateTerms {
    FIXED,
    TIERED,
    TIERED_PERIOD,
    TIERED_BAND,
}