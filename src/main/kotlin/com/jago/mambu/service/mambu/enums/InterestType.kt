package com.jago.mambu.service.mambu.enums

enum class InterestType {
    SIMPLE_INTEREST,
    CAPITALIZED_INTEREST,
    COMPOUNDING_INTEREST,
}