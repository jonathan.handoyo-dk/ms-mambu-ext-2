package com.jago.mambu.service.mambu.enums

enum class LatePaymentsRecalculationMethod {
    OVERDUE_INSTALLMENTS_INCREASE,
    LAST_INSTALLMENT_INCREASE,
}