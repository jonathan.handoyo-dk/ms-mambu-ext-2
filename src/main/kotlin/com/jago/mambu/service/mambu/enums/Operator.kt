package com.jago.mambu.service.mambu.enums

enum class Operator {
    EQUALS,
    EQUALS_CASE_SENSITIVE,
    DIFFERENT_THAN,
    MORE_THAN,
    LESS_THAN,
    BETWEEN,
    ON,
    AFTER,
    AFTER_INCLUSIVE,
    BEFORE,
    BEFORE_INCLUSIVE,
    STARTS_WITH,
    STARTS_WITH_CASE_SENSITIVE,
    IN,
    TODAY,
    THIS_WEEK,
    THIS_MONTH,
    THIS_YEAR,
    LAST_DAYS,
    EMPTY,
    NOT_EMPTY
}