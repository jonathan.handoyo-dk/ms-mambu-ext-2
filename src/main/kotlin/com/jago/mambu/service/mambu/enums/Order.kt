package com.jago.mambu.service.mambu.enums

enum class Order {
    ASC,
    DESC
}