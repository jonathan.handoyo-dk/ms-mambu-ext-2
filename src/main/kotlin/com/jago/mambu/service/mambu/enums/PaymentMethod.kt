package com.jago.mambu.service.mambu.enums

enum class PaymentMethod {
    HORIZONTAL,
    VERTICAL,
}