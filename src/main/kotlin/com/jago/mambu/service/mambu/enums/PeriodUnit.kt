package com.jago.mambu.service.mambu.enums

enum class PeriodUnit {
    DAYS,
    WEEKS,
    MONTHS,
    YEARS,
}