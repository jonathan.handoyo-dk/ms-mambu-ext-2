package com.jago.mambu.service.mambu.enums

enum class PrincipalPaidInstallmentStatus {
    PARTIALLY_PAID,
    PAID,
    ORIGINAL_TOTAL_EXPECTED_PAID,
}