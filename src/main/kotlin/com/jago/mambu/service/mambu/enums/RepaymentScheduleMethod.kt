package com.jago.mambu.service.mambu.enums

enum class RepaymentScheduleMethod {
    NONE,
    FIXED,
    DYNAMIC,
}