package com.jago.mambu.service.mambu.enums

enum class Requirement {
    OPTIONAL,
    REQUIRED,
    NOT_REQUIRED,
}