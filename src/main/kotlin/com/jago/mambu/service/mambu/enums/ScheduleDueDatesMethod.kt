package com.jago.mambu.service.mambu.enums

enum class ScheduleDueDatesMethod {
    INTERVAL,
    FIXED_DAYS_OF_MONTH,
}