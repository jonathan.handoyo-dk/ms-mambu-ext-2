package com.jago.mambu.service.mambu.enums

enum class ShortMonthHandlingMethod {
    LAST_DAY_IN_MONTH,
    FIRST_DAY_OF_NEXT_MONTH,
}