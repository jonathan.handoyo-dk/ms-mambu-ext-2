package com.jago.mambu.service.mambu.enums

enum class Toggle {
    ON,
    OFF,
}