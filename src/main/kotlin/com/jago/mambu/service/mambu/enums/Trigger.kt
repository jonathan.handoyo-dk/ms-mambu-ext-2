package com.jago.mambu.service.mambu.enums

enum class Trigger {
    MANUAL,
    MONTHLY_FEE,
    ARBITRARY,
}