package com.jago.mambu.service.mambu.models

import org.springframework.http.HttpHeaders

data class PaginationDetails(
    val offset: Int,
    val limit: Int,
    val total: Int?,
) {
    constructor(offset: Int, limit: Int): this(offset, limit, null)

    constructor(headers: HttpHeaders): this(
        offset = headers.getFirst("Items-Offset")?.toInt() ?: -1,
        limit = headers.getFirst("Items-Limit")?.toInt() ?: -1,
        total = headers.getFirst("Items-Total")?.toInt(),
    )
}
