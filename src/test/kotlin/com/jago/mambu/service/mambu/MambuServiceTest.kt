package com.jago.mambu.service.mambu

import com.jago.mambu.service.mambu.dtos.*
import com.jago.mambu.service.mambu.models.PaginationDetails
import com.jago.mambu.toLocalDate
import com.jago.mambu.util.*
import io.mockk.junit5.MockKExtension
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpStatus
import java.time.Instant

@ExtendWith(MockKExtension::class)
@Suppress("KotlinConstantConditions", "UNNECESSARY_NOT_NULL_ASSERTION")
internal class MambuServiceTest {

    private val mambuWebServer = MockWebServer()
    private val mambuRestTemplate = RestTemplateBuilder().build()
    private val mambuProperties = MambuProperties(MambuProperties.Api(mambuWebServer.url("/").toString(), random.next(), 4))

    private val mambuService = MambuService(mambuRestTemplate, mambuProperties)

    @Test
    fun givenMambuResponds401_shouldThrow() {
        // Given
        val limit = mambuProperties.api.limit
        mambuWebServer.enqueueJson(HttpStatus.UNAUTHORIZED, PaginationDetails((0 * limit), limit), ClassPathResource("mocks/error-401.json"))

        // When-Then
        assertThatExceptionOfType(MambuException::class.java)
            .isThrownBy { mambuService.getBranches() } // <- should be able to use any API
            .withMessageContaining("Status: 401")
            .withMessageContaining("INVALID_CREDENTIALS")
    }

    @Test
    fun givenMambuResponds400_shouldThrow() {
        // Given
        val limit = mambuProperties.api.limit
        mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit, 1), null)
        mambuWebServer.enqueueJson(HttpStatus.BAD_REQUEST, PaginationDetails((0 * limit), limit), ClassPathResource("mocks/error-400.json"))

        // When-Then
        assertThatExceptionOfType(MambuException::class.java)
            .isThrownBy { mambuService.getBranches() }
            .withMessageContaining("Status: 400")
            .withMessageContaining("INVALID_PAGINATION_OFFSET_VALUE") // <- just a sample that throws 400
    }

    @Nested
    inner class GetBranches {

        @Test
        fun given200FromMambu_shouldReturn() {
            // Given
            val limit = mambuProperties.api.limit
            val expected = readObject<List<Branch>>("mocks/get-branches-200-response-total.json")
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit, expected.size), null)
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit), ClassPathResource("mocks/get-branches-200-response-p1.json"))
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((1 * limit), limit), ClassPathResource("mocks/get-branches-200-response-p2.json"))
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((2 * limit), limit), ClassPathResource("mocks/get-branches-200-response-p3.json"))

            // When
            val actual = mambuService.getBranches()

            // Then
            assertThat(actual).isEqualTo(expected)
            assertThat(mambuWebServer.takeAllRequests())
                .hasSize(4)
                .allSatisfy { assertThat(it.requestUrl.toString()).contains("/api/branches") }
        }
    }

    @Nested
    inner class GetBranchByEncodedKey {

        @Test
        fun given200FromMambu_shouldReturn() {
            // Given
            val expected = readObject<Branch>("mocks/get-branch-200-response.json")
            mambuWebServer.enqueueJson(HttpStatus.OK, null, expected)

            // When
            val actual = mambuService.getBranchByEncodedKey(expected.encodedKey!!)

            // Then
            assertThat(actual).isEqualTo(expected)
            assertThat(mambuWebServer.requestCount).isEqualTo(1)
            assertThat(mambuWebServer.takeRequest().requestUrl.toString()).contains("/api/branches/${expected.encodedKey!!}")
        }

        @Test
        fun given404FromMambu_shouldThrow() {
            // Given
            mambuWebServer.enqueueJson(HttpStatus.NOT_FOUND, null, ClassPathResource("mocks/get-branch-404-response.json"))

            // When-Then
            assertThatExceptionOfType(MambuException::class.java)
                .isThrownBy { mambuService.getBranchByEncodedKey(random.next()) }
                .withMessageContaining("Status: 404")
                .withMessageContaining("INVALID_BRANCH_ID")

            assertThat(mambuWebServer.takeRequest().requestUrl.toString()).contains("/api/branches")
        }
    }

    @Nested
    inner class GetDepositProducts {

        @Test
        fun given200FromMambu_shouldReturn() {
            // Given
            val limit = mambuProperties.api.limit
            val expected = readObject<List<DepositProduct>>("mocks/get-deposit-products-200-response-total.json")
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit, expected.size), null)
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit), ClassPathResource("mocks/get-deposit-products-200-response-p1.json"))
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((1 * limit), limit), ClassPathResource("mocks/get-deposit-products-200-response-p2.json"))
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((2 * limit), limit), ClassPathResource("mocks/get-deposit-products-200-response-p3.json"))

            // When
            val actual = mambuService.getDepositProducts()

            // Then
            assertThat(actual).isEqualTo(expected)
            assertThat(mambuWebServer.takeAllRequests())
                .hasSize(4)
                .allSatisfy { assertThat(it.requestUrl.toString()).contains("/api/depositproducts") }
        }
    }

    @Nested
    inner class SearchCreditArrangements {

        @Test
        fun given200FromMambu_shouldReturn() {
            // Given
            val limit = mambuProperties.api.limit
            val expected = readObject<List<CreditArrangement>>("mocks/search-credit-arrangements-200-response-total.json")
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit, expected.size), null)
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit), ClassPathResource("mocks/search-credit-arrangements-200-response-p1.json"))
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((1 * limit), limit), ClassPathResource("mocks/search-credit-arrangements-200-response-p2.json"))
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((2 * limit), limit), ClassPathResource("mocks/search-credit-arrangements-200-response-p3.json"))

            // When
            val criteria = readObject<SearchCriteria>("mocks/search-credit-arrangements-request.json")
            val date = criteria.filterCriteria!!.first { it.field == "expireDate" }.value!!.let { Instant.parse(it) }.toLocalDate()
            val actual = mambuService.searchCreditArrangementsAfter(date)

            // Then
            assertThat(actual).isEqualTo(expected)
            assertThat(mambuWebServer.takeAllRequests())
                .hasSize(4)
                .allSatisfy {
                    assertThat(it.requestUrl.toString()).contains("/api/creditarrangements:search")
                    assertThat(it.readBody<SearchCriteria>()).isEqualTo(criteria)
                }
        }
    }

    @Nested
    inner class SearchLoans {

        @Test
        fun given200FromMambu_shouldReturn() {
            // Given
            val limit = mambuProperties.api.limit
            val expected = readObject<List<LoanAccountFullDetails>>("mocks/search-loans-200-response-total.json")
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit, expected.size), null)
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((0 * limit), limit), ClassPathResource("mocks/search-loans-200-response-p1.json"))
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((1 * limit), limit), ClassPathResource("mocks/search-loans-200-response-p2.json"))
            mambuWebServer.enqueueJson(HttpStatus.OK, PaginationDetails((2 * limit), limit), ClassPathResource("mocks/search-loans-200-response-p3.json"))

            // When
            val criteria = readObject<SearchCriteria>("mocks/search-loans-request.json")

            val productTypeKey = criteria.filterCriteria!!.first { it.field == "productTypeKey" }.value!!
            val dateFrom = criteria.filterCriteria!!.first { it.field == "creationDate" }.value!!.let { Instant.parse(it) }.toLocalDate()
            val dateTo = criteria.filterCriteria!!.first { it.field == "creationDate" }.secondValue!!.let { Instant.parse(it) }.toLocalDate()
            val states = criteria.filterCriteria!!.first { it.field == "accountState" }.values!!.map { LoanAccountFullDetails.State.valueOf(it) }

            val actual = mambuService.searchLoans(productTypeKey, dateFrom, dateTo, states)

            // Then
            assertThat(actual).isEqualTo(expected)
            assertThat(mambuWebServer.takeAllRequests())
                .hasSize(4)
                .allSatisfy {
                    assertThat(it.requestUrl.toString()).contains("/api/loans:search")
                    assertThat(it.readBody<SearchCriteria>()).isEqualTo(criteria)
                }
        }
    }
}