package com.jago.mambu.util

import com.fasterxml.jackson.module.kotlin.readValue
import com.jago.mambu.service.mambu.models.PaginationDetails
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus

fun <T> MockWebServer.enqueueJson(status: HttpStatus, paginationDetails: PaginationDetails? = null, payload: T? = null) {
    val body: String? = when (payload) {
        null -> null
        is String -> payload
        is ClassPathResource -> payload.file.readText()
        else -> objectMapper.writeValueAsString(payload)
    }

    val response = MockResponse()
        .setResponseCode(status.value())
        .addHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.mambu.v2+json")
        .addHeader(HttpHeaders.CONNECTION, "close")
        .apply {
            if (paginationDetails != null) {
                addHeader("items-offset", paginationDetails.offset)
                addHeader("items-limit", paginationDetails.limit)
                if (paginationDetails.total != null) {
                    addHeader("items-total", paginationDetails.total)
                }
            }
        }
    if (body != null) response.setBody(body)

    this.enqueue(response)
}

inline fun <reified T : Any> RecordedRequest.readBody(): T = objectMapper.readValue(this.body.readUtf8())

fun MockWebServer.takeAllRequests(): List<RecordedRequest> {
    return (1..this.requestCount)
        .map { this.takeRequest() }
        .toList()
}