package com.jago.mambu.util

import org.jeasy.random.EasyRandom

val random: EasyRandom = EasyRandom()

inline fun <reified T> EasyRandom.next(): T = this.nextObject(T::class.java)